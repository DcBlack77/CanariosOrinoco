var express = require('express');

var bodyParser = require('body-parser');
var multer = require('multer');
var mysql = require('mysql');
var pass_app = "123";
var method_override = require("method-override");
var uploader = multer({ dest: "./uploads" });
var middleware_upload = uploader.single('jpg');
var cloudinary = require('cloudinary');

cloudinary.config({
    cloud_name: "dcblack77",
    api_key: "229868825344196",
    api_secret: "AJh_v0gWZ8z0gamAKzSdl_izWyQ"

});

//var force = require ("express-force-domain");

var app = express();

//mongoose.connect("mongodb://localhost/canariosdb");
var connectionDB = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '20773651',
    database: 'canariosdb',
    port: 3306
});


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(method_override("_method"));
//app.use(force('http://bocdelivery.com.ve'));


app.set("view engine", "pug");

app.use(express.static("staticpublic"));

app.get("/", function(req, res) {
    res.render("index");
});


app.get("/admin", function(req, res) {
    res.render("admin/form");
});

app.post("/admin", function(req, res) {
    if (req.body.password == pass_app) {
        connectionDB.query('select * from persona', function(err, doct) {
            if (err) { console.log(err); }
            res.render("admin/index", { personas: doct })
        });
    } else {
        res.render("error/index");
    }
});

app.get("/comunidad_espanola", function(req, res) {
    res.render("registros/comunidad_e");
});

app.post("/comunidades_espanolas", function(req, res) {
    var data = {
        comunidad: req.body.comunidad,
        provincia: req.body.provincia,
        isla: req.body.isla,
        municipio: req.body.municipio
    }

    connectionDB.query('insert into comunidad_canaria set ?', data, function(err, res) {
        if (err) {
            console.log(err);
            return;
        } else {
            console.log(data)
        }
    });

    // var canarias = new Canarias(data);
    //
    // canarias.save(function(err){
    // 	console.log(canarias);
    // 	res.redirect("comunidad_espanola");
    // });
});

app.get("/error", function(req, res) {
    res.render("error/index");
});

app.post("/", middleware_upload, function(req, res) {

    var data = {
        nombre: req.body.nombre,
        apellido1: req.body.apellido1,
        apellido2: req.body.apellido2,
        nacimiento: req.body.nacimiento,
        cdi: req.body.cdi,
        passport: req.body.passport,
        nic: req.body.nic,
        email: req.body.email,
        tlf: req.body.tlf,
        municipio_n: req.body.municipio_n,
        isla_n: req.body.isla_n,
        provincia_n: req.body.provincia_n,
        comunidad_n: req.body.comunidad_n,
        calle_a: req.body.calle_a,
        tipo_v: req.body.tipo_v,
        ciudad_a: req.body.ciudad_a,
        estado_a: req.body.estado_a,
        idpostal_a: req.body.idpostal_a,
        nacionalidad: req.body.nacionalidad,
        pension_e: req.body.pension_e,
        ayuda_d: req.body.ayuda_d,
        fes: req.body.fes,
        ivss: req.body.ivss,
        ayuda_o: req.body.ayuda_o

    }

    connectionDB.query('insert into persona set ?', data, function(err, res) {
        if (err) {
            console.log(err);
            return;
        } else {
            console.log(data)
        }
    });
    res.redirect("/")


});

app.get("/error/registro", function(res, req) {
    res.render("error/registro");
});

app.get("/menu/edit/:id", function(req, res) {
    var id_p = req.params.id;
    console.log(id_p);
    connectionDB.query("SELECT * FROM persona WHERE id ?", id_p, function(err, per) {
        console.log(per);
        res.render("/menu/edit", { persona: per });
    });

});

app.put("/menu/:id", middleware_upload, function(req, res) {

});


app.listen(8001);

console.log("Corriendo en el puerto 8001");
